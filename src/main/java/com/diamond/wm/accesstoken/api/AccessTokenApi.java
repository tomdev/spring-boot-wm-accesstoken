package com.diamond.wm.accesstoken.api;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.diamond.wm.accesstoken.entity.AccessToken;
import com.diamond.wm.accesstoken.exception.WechatRequestException;
import com.diamond.wm.accesstoken.manager.AccessTokenManager;

@RestController
@RequestMapping("token")
public class AccessTokenApi {
	
	@Resource
	AccessTokenManager manager;
	
	@RequestMapping("get")
	public AccessToken getToken(String appid,String secret) throws WechatRequestException{
		AccessToken token = manager.getToken(appid,secret);
		return token;
	}
	
	@RequestMapping("getstr")
	public String getTokenOnly(String appid,String secret) throws WechatRequestException{
		return getToken(appid, secret).getAccessToken();
	}

	@RequestMapping("remove")
	public String remove(String appid,String secret){
		manager.removeToken(appid, secret);
		return "success";
	}
	
	@RequestMapping("flush")
	public AccessToken flush(String appid,String secret) throws WechatRequestException{
		return manager.flushToken(appid, secret);
	}
	
	@ExceptionHandler(WechatRequestException.class)
	public String wechatRequestExceptionHandler(WechatRequestException ex){
		return ex.getMessage();
	}
}
